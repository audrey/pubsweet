# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="2.5.5"></a>
## [2.5.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.5.4...pubsweet-client@2.5.5) (2018-05-21)




**Note:** Version bump only for package pubsweet-client

<a name="2.5.4"></a>
## [2.5.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.5.3...pubsweet-client@2.5.4) (2018-05-18)


### Bug Fixes

* use one file at monorepo root ([456f49b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/456f49b))




<a name="2.5.3"></a>
## [2.5.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.5.2...pubsweet-client@2.5.3) (2018-05-10)




**Note:** Version bump only for package pubsweet-client

<a name="2.5.2"></a>
## [2.5.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.5.1...pubsweet-client@2.5.2) (2018-05-09)




**Note:** Version bump only for package pubsweet-client

<a name="2.5.1"></a>
## [2.5.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.5.0...pubsweet-client@2.5.1) (2018-05-03)




**Note:** Version bump only for package pubsweet-client

<a name="2.5.0"></a>
# [2.5.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.4.1...pubsweet-client@2.5.0) (2018-04-25)


### Features

* **client:** add support for Team SSE ([429d113](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/429d113))




<a name="2.4.1"></a>
## [2.4.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.4.0...pubsweet-client@2.4.1) (2018-04-24)


### Bug Fixes

* **authenticated-component:** redirect to next path ([fbde445](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fbde445))




<a name="2.4.0"></a>
# [2.4.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.3.1...pubsweet-client@2.4.0) (2018-04-11)


### Features

* enable the Apollo client to be customised ([0546acc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0546acc))




<a name="2.3.1"></a>
## [2.3.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.3.0...pubsweet-client@2.3.1) (2018-03-30)




**Note:** Version bump only for package pubsweet-client

<a name="2.3.0"></a>
# [2.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.2.6...pubsweet-client@2.3.0) (2018-03-28)


### Bug Fixes

* **client:** add tests to graphql HOCs ([440bb4b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/440bb4b))


### Features

* **client:** add Apollo Client ([2fe9d93](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2fe9d93))




<a name="2.2.6"></a>
## [2.2.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.2.5...pubsweet-client@2.2.6) (2018-03-28)




**Note:** Version bump only for package pubsweet-client

<a name="2.2.5"></a>
## [2.2.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.2.4...pubsweet-client@2.2.5) (2018-03-27)




**Note:** Version bump only for package pubsweet-client

<a name="2.2.4"></a>
## [2.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.2.3...pubsweet-client@2.2.4) (2018-03-19)




**Note:** Version bump only for package pubsweet-client

<a name="2.2.3"></a>
## [2.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.2.2...pubsweet-client@2.2.3) (2018-03-15)




**Note:** Version bump only for package pubsweet-client

<a name="2.2.2"></a>

## [2.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.2.1...pubsweet-client@2.2.2) (2018-03-09)

**Note:** Version bump only for package pubsweet-client

<a name="2.2.1"></a>

## [2.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.2.0...pubsweet-client@2.2.1) (2018-03-06)

### Bug Fixes

* **client:** add ui dependency ([cc1a8ae](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cc1a8ae))

<a name="2.2.0"></a>

# [2.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.1.1...pubsweet-client@2.2.0) (2018-03-05)

### Bug Fixes

* downgrade styled-components dependency in pubsweet-client ([718558e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/718558e))
* update Root to use new StyleRoot component ([9d4c0ef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9d4c0ef))

### Features

* **normalize:** add normalize css ([9eb24e5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9eb24e5))
* **ui:** add theming to Tags ([ee959d2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee959d2))

<a name="2.1.1"></a>

## [2.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.1.0...pubsweet-client@2.1.1) (2018-02-16)

### Bug Fixes

* **client:** remove unused dependency on login component ([6c5dd97](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6c5dd97))

<a name="2.1.0"></a>

# [2.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.0.0...pubsweet-client@2.1.0) (2018-02-08)

### Features

* **client:** add styled components ([43ab2c5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/43ab2c5))

<a name="2.0.0"></a>

# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@1.1.4...pubsweet-client@2.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
