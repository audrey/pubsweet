# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.1"></a>
## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@1.0.0...@pubsweet/coko-theme@1.0.1) (2018-05-09)


### Bug Fixes

* **theme:** fix active underline for action on coko theme ([9ef7165](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9ef7165))




<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@0.1.1...@pubsweet/coko-theme@1.0.0) (2018-05-03)


### Bug Fixes

* **theme:** enable boxShadow, even if it is not currently used anywhere ([f2c5538](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f2c5538))
* **theme:** remove hardcoded color ([4786945](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4786945))
* **theme:** remove warning color ([c0897c8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c0897c8))
* **theme:** update theme colors ([a32b92f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a32b92f))
* **xpub-dashboard:** correct styles for author manuscripts ([1d8761e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1d8761e))


### Features

* **theme:** coko theme is in place ([731f501](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/731f501))
* **ui:** add action element ([301d800](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/301d800))
* **ui:** add action group ([32b9555](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/32b9555))


### BREAKING CHANGES

* **theme:** might break components that used the warning colors




<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@0.1.0...@pubsweet/coko-theme@0.1.1) (2018-04-11)




**Note:** Version bump only for package @pubsweet/coko-theme

<a name="0.1.0"></a>
# 0.1.0 (2018-03-28)


### Features

* **theme:** add a Coko theme ([0c503cf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0c503cf))
