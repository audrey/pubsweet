import { css } from 'styled-components'

export default {
  Label: css`
    font-style: italic;
    letter-spacing: 1px;

    span {
      font-size: 1.1em;
    }
  `,
}
