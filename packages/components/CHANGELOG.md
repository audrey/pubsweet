# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

  <a name="6.3.0"></a>
# [6.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@6.2.2...pubsweet-components@6.3.0) (2018-05-24)


### Bug Fixes

* **aws-s3:** add end when sending 204 ([5a72d40](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5a72d40))
* **aws-s3:** handle case when no files were found ([36241c2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/36241c2))


### Features

* **aws-s3:** file download and select zip file types ([e4a876f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e4a876f))




  <a name="6.2.2"></a>
## [6.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@6.2.1...pubsweet-components@6.2.2) (2018-05-21)


### Bug Fixes

* **components:** use Tabs from pubsweet ui ([8e9fd3c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8e9fd3c))




<a name="6.2.1"></a>
## [6.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@6.2.0...pubsweet-components@6.2.1) (2018-05-18)


### Bug Fixes

* use MIT on all package.json files ([4558ae4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4558ae4))
* **components:** add tests to suggestions component ([50777b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/50777b3))
* **components:** authors assigning problem ([50baa94](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/50baa94))
* **components:** cases with empty editors suggestions ([0a6bd45](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0a6bd45))
* **components:** delete unneeded line ([daea008](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/daea008))
* **components:** rewrite conditional checks to more clean ([c41d79d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c41d79d))
* **components:** submit submitted versions ([48d07ee](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/48d07ee))
* **components:** upload diasble during converting ([227b136](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/227b136))




<a name="6.2.0"></a>
# [6.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@6.1.0...pubsweet-components@6.2.0) (2018-05-10)


### Bug Fixes

* **components:** dashboard if statment reject ([999587a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/999587a))
* **components:** decision linter ([5679ce0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5679ce0))
* **components:** fix lint errors ([4e22ec1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4e22ec1))
* **components:** fix linting issues ([4385b58](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4385b58))
* **components:** fixes in linter ([7c31f6b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c31f6b))
* **components:** html parse, styled components ([8b24552](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8b24552))
* **components:** lint issues ([ff56878](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ff56878))
* **components:** linter ([9aac3fa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9aac3fa))
* **components:** merge two commponets two one ([4e2ed76](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4e2ed76))
* **components:** redirect submission add selectors ([53db5a7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/53db5a7))
* **components:** redux form ([2f7f1ed](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2f7f1ed))
* **components:** review page layout ([4ea2cdd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4ea2cdd))
* **components:** take care of case of zero files ([82cff08](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/82cff08))
* **components:** title wording ([0c293f4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0c293f4))


### Features

* **components:** add columns to submission and tabs ([40470a0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/40470a0))
* **components:** add current version files ([4c77f3c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4c77f3c))
* **components:** add tabs to submission ([0e45892](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0e45892))
* **components:** create accordion component ([05a23e4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/05a23e4))
* **components:** create accordion component ([54f5b7d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/54f5b7d))




<a name="6.1.0"></a>
# [6.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@6.0.0...pubsweet-components@6.1.0) (2018-05-09)


### Bug Fixes

* **xpub-dash:** fix reviewer item crash when status is revision ([fc79496](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fc79496))
* fixed misnamed redux form props in authors input ([940edc0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/940edc0))
* fixed misnamed redux form props in authors input ([bb4af56](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bb4af56))
* fixed misnamed redux form props in authors input ([fb362b2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fb362b2))
* **xpub-edit:** ensure config is not regenerated on each render ([d98e722](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d98e722))


### Features

* add AuthorsInput component ([f7d12b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f7d12b3))
* authors input, added padding around fields ([1e5d742](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1e5d742))
* authors input, fixed merge error ([c908fa4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c908fa4))
* authors input, fixed prettier errors ([0657143](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0657143))
* authors input,component  updated to ensure at least one author ([d43dd92](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d43dd92))
* list styles for authors input ([3f85bbd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3f85bbd))
* two inputs per line ([aa0544a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aa0544a))
* update MetadataFields to use AuthorsInput component ([fa1640e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fa1640e))
* update MetadataFields to use AuthorsInput component ([1baac87](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1baac87))
* update MetadataFields to use AuthorsInput component ([355f282](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/355f282))




<a name="6.0.0"></a>
# [6.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@5.4.0...pubsweet-components@6.0.0) (2018-05-03)


### Bug Fixes

* **theme:** remove warning color ([c0897c8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c0897c8))
* **xpub-dashboard:** correct styles for author manuscripts ([1d8761e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1d8761e))


### BREAKING CHANGES

* **theme:** might break components that used the warning colors




<a name="5.4.0"></a>
# [5.4.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@5.3.2...pubsweet-components@5.4.0) (2018-05-03)


### Bug Fixes

* **components:** add version to collection ([98303ae](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98303ae))
* **components:** align columns cp page ([a5968b0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a5968b0))
* **components:** align dropdown horizontally ([a7081e3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a7081e3))
* **components:** change position and direction of assign ([7a7eeb3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7a7eeb3))
* **components:** change supplymentary to attachment component ([143c452](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/143c452))
* **components:** fix lint errors ([c2b8e52](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2b8e52))
* **components:** fix lint errors ([2503ff9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2503ff9))
* **components:** fix lint errors ([98046fb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98046fb))
* **components:** fix lint errors ([be173db](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be173db))
* **components:** fix unsued lint error ([20c282c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/20c282c))
* **components:** load all users to control panel ([92dac6b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/92dac6b))
* **components:** load all users to control panel ([90c88e6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/90c88e6))
* **components:** load all users to control panel ([85fa14f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85fa14f))
* **components:** load all users to control panel ([f20e44d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f20e44d))
* **components:** loading data in the decision form ([8f499aa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8f499aa))
* **components:** remove from Dashboard assign editor ([751a63e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/751a63e))
* **components:** take care of case of zero files ([b70f728](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b70f728))
* **xpub-edit:** prevent editors from submitting form ([f1f7dda](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f1f7dda))


### Features

* **components:** add assign editors to cp ([3cca44d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3cca44d))
* **components:** add assign editors to cp ([987310d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/987310d))
* **xpub-edit:** create editor component with simple API ([ee6fb83](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee6fb83))




<a name="5.3.2"></a>
## [5.3.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@5.3.1...pubsweet-components@5.3.2) (2018-04-25)




**Note:** Version bump only for package pubsweet-components

<a name="5.3.1"></a>
## [5.3.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@5.3.0...pubsweet-components@5.3.1) (2018-04-25)




**Note:** Version bump only for package pubsweet-components

<a name="5.3.0"></a>
# [5.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@5.2.0...pubsweet-components@5.3.0) (2018-04-24)


### Bug Fixes

* **aws-s3:** update getSigned url ([d6f27ef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d6f27ef))
* **compoenents:** fix cases of empty objects in metadata ([7a5bfbc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7a5bfbc))
* **component:** put striphtml function back to place ([2a69dca](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2a69dca))
* **components:** add file streamlined data ([9dd6797](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9dd6797))
* **components:** add metadata StreamLined ([29a1fcd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/29a1fcd))
* **components:** add subinfo to upload ([446fc16](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/446fc16))
* **components:** change order ([2020d49](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2020d49))
* **components:** change placeholder ([d80a41a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d80a41a))
* **components:** change text input fields ([775a961](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/775a961))
* **components:** change text to create submission button ([d3b6385](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d3b6385))
* **components:** change value to files at upload components ([aa2b45e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aa2b45e))
* **components:** dashboard update ([08feafb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/08feafb))
* **components:** fix file name problem ([73f33c9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/73f33c9))
* **components:** fix review backend test ([874b6a3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/874b6a3))
* **components:** fix test backend authbear ([25c0623](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/25c0623))
* **components:** fix test backend authbear ([7c16970](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c16970))
* **components:** passport through route ([593eeda](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/593eeda))
* **components:** passport through route ([fdf9dce](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fdf9dce))
* **components:** remove tables - add list of metadata ([b9d57cd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b9d57cd))
* **components:** remove xpub-app folder ([d356c6d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d356c6d))
* **components:** statuses changed for revision ([3bc09dc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3bc09dc))
* **components:** structure title under Toolbar ([3e7b76b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3e7b76b))
* **dashboard:** change stremlined metadata label ([992cc4f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/992cc4f))
* **xpub-submit:** use no-redux version of uploadFile ([cc904a3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cc904a3))


### Features

* **component:** add make invitation request ([9e00e11](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9e00e11))
* **component:** add make invitation request ([30193b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/30193b3))
* **component:** add make invitation request ([7245d35](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7245d35))
* **component:** add make invitation request ([947c846](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/947c846))
* **component:** add make invitation request ([ae785b9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ae785b9))
* **component:** add make invitation request ([36faf21](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/36faf21))
* **component:** add make invitation request ([38e5728](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/38e5728))
* **component:** add make invitation request ([bda7d95](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bda7d95))
* **component:** add make invitation request ([1911bab](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1911bab))
* **component:** add make invitation request ([335d0f0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/335d0f0))
* **component:** add make invitation request ([1412d87](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1412d87))
* **component:** add make invitation request ([d070bb1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d070bb1))
* **component:** add make invitation request ([f9cae33](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f9cae33))
* **component:** add make invitation request ([61a1f0b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/61a1f0b))
* **component:** add make invitation request ([5a9b393](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5a9b393))
* **component:** add make invitation request ([430e9e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/430e9e2))
* **component:** add make invitation request ([3d13943](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3d13943))
* **component:** add make invitation request ([27e2984](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/27e2984))
* **component:** add make invitation request ([b049aa3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b049aa3))
* **component:** add make invitation request ([c4317bb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c4317bb))
* **component:** add make invitation request ([dcd1f46](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/dcd1f46))
* **component:** add make invitation request ([9d731d3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9d731d3))
* **component:** add make invitation request ([9d0ad57](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9d0ad57))
* **component:** add make invitation request ([20628a4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/20628a4))
* **component:** add make invitation request ([44f1574](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/44f1574))
* **component:** add make invitation request ([6f27a3e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6f27a3e))
* **component:** add make invitation request ([3817c8a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3817c8a))
* **component:** add make invitation request ([659eb64](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/659eb64))
* **component:** add make invitation request ([ff3f8fb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ff3f8fb))
* **component:** add make invitation request ([4820e45](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4820e45))
* **component:** add make invitation request ([721bbaf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/721bbaf))
* **component:** add make invitation request ([251381c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/251381c))
* **components:** add API endpoint invitation ([009b693](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/009b693))
* **components:** add API endpoint invitation ([9276ef9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9276ef9))
* **components:** add API endpoint invitation ([ee82d6b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee82d6b))
* **components:** add API endpoint invitation ([e50383f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e50383f))
* **components:** add API endpoint invitation ([fc40c17](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fc40c17))
* **components:** add API endpoint invitation ([a781136](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a781136))
* **components:** add API endpoint invitation ([24810b8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/24810b8))
* **components:** add API endpoint invitation ([d21afe6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d21afe6))
* **components:** add API endpoint invitation ([2703f3e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2703f3e))
* **components:** add API endpoint invitation ([24ee6ca](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/24ee6ca))
* **components:** add API endpoint invitation ([17d9532](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/17d9532))
* **components:** add API endpoint invitation ([98d568e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98d568e))
* **components:** add API endpoint invitation ([d19bf84](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d19bf84))
* **components:** add API endpoint invitation ([4739b84](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4739b84))
* **components:** add API endpoint invitation ([f963b76](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f963b76))
* **components:** add API endpoint invitation ([10fb6e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/10fb6e2))
* **components:** add API endpoint invitation ([c6d0d94](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c6d0d94))
* **components:** add API endpoint invitation ([16bf6a1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/16bf6a1))
* **components:** add API endpoint invitation ([d897187](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d897187))
* **components:** add API endpoint invitation ([19932d7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/19932d7))
* **components:** add API endpoint invitation ([3c5cbea](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3c5cbea))
* **components:** add API endpoint invitation ([8de6954](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8de6954))
* **components:** add API endpoint invitation ([9e9b4ce](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9e9b4ce))
* **components:** add API endpoint invitation ([daff65a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/daff65a))
* **components:** add API endpoint invitation ([d06ae4b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d06ae4b))
* **components:** add API endpoint invitation ([0ca4cfa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0ca4cfa))
* **components:** add API endpoint invitation ([ae7d9aa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ae7d9aa))
* **components:** add API endpoint invitation ([d6bb84a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d6bb84a))
* **components:** add API endpoint invitation ([3fcc322](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3fcc322))
* **components:** add API endpoint invitation ([db805c4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/db805c4))
* **components:** add API endpoint invitation ([48e8e12](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/48e8e12))
* **components:** add API endpoint invitation ([abdf121](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/abdf121))
* **components:** create invite reviewer endpoints ([17fdfdc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/17fdfdc))
* **components:** create invite reviewer endpoints ([9e3fdef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9e3fdef))
* **components:** create invite reviewer endpoints ([794918c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/794918c))
* **components:** create invite reviewer endpoints ([7c35ccc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c35ccc))
* **components:** create invite reviewer endpoints ([c603b77](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c603b77))
* **components:** create invite reviewer endpoints ([b2f627f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b2f627f))
* **components:** create invite reviewer endpoints ([fc6cad4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fc6cad4))
* **components:** create invite reviewer endpoints ([1f57c82](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1f57c82))
* **components:** create invite reviewer endpoints ([8774faa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8774faa))
* **components:** create invite reviewer endpoints ([a47ddf4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a47ddf4))
* **components:** create invite reviewer endpoints ([4b21241](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4b21241))
* **components:** create invite reviewer endpoints ([75e89c8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/75e89c8))
* **components:** create invite reviewer endpoints ([27ec4f8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/27ec4f8))
* **components:** create invite reviewer endpoints ([cfcc711](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cfcc711))
* **components:** create invite reviewer endpoints ([cd83ae9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cd83ae9))
* **components:** create invite reviewer endpoints ([42d65cc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/42d65cc))
* **components:** create invite reviewer endpoints ([442c191](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/442c191))
* **components:** create invite reviewer endpoints ([ea8a42e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ea8a42e))
* **components:** create invite reviewer endpoints ([19a48be](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/19a48be))
* **components:** create invite reviewer endpoints ([74b1ebc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/74b1ebc))
* **components:** create invite reviewer endpoints ([f8e53c7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f8e53c7))
* **components:** create invite reviewer endpoints ([4269fbe](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4269fbe))
* **components:** create invite reviewer endpoints ([4c10e2a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4c10e2a))
* **components:** create invite reviewer endpoints ([e3919ce](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e3919ce))
* **components:** create invite reviewer endpoints ([304ff02](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/304ff02))
* **components:** create invite reviewer endpoints ([f7d7ec2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f7d7ec2))
* **components:** create invite reviewer endpoints ([1b29a5c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1b29a5c))
* **components:** create invite reviewer endpoints ([0e72b91](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0e72b91))
* **components:** create invite reviewer endpoints ([c1d487f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c1d487f))
* **components:** create invite reviewer endpoints ([df75c8f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/df75c8f))
* **components:** create invite reviewer endpoints ([20d456b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/20d456b))
* **components:** create invite reviewer endpoints ([f464beb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f464beb))
* **components:** fix correct update of reviewer ([c1b734e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c1b734e))
* **components:** fix correct update of reviewer ([2476402](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2476402))
* **components:** fix correct update of reviewer ([5988b36](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5988b36))
* **components:** fix correct update of reviewer ([dcd2f94](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/dcd2f94))
* **components:** fix correct update of reviewer ([895bc73](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/895bc73))
* **components:** fix correct update of reviewer ([d7553ef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d7553ef))
* **components:** fix correct update of reviewer ([1b449ab](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1b449ab))
* **components:** fix correct update of reviewer ([3d2d412](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3d2d412))
* **components:** fix correct update of reviewer ([358a8f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/358a8f6))
* **components:** fix correct update of reviewer ([45b7375](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/45b7375))
* **components:** fix correct update of reviewer ([11c44ae](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/11c44ae))
* **components:** fix correct update of reviewer ([ec31850](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ec31850))
* **components:** fix correct update of reviewer ([b22a250](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b22a250))
* **components:** fix correct update of reviewer ([46fdc54](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/46fdc54))
* **components:** fix correct update of reviewer ([6a423e4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6a423e4))
* **components:** fix correct update of reviewer ([ee775be](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee775be))
* **components:** fix correct update of reviewer ([c030cce](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c030cce))
* **components:** fix correct update of reviewer ([30559e0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/30559e0))
* **components:** fix correct update of reviewer ([acd3a2d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/acd3a2d))
* **components:** fix correct update of reviewer ([b937db4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b937db4))
* **components:** fix correct update of reviewer ([48a70a8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/48a70a8))
* **components:** fix correct update of reviewer ([d5de1d0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d5de1d0))
* **components:** fix correct update of reviewer ([b42550f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b42550f))
* **components:** fix correct update of reviewer ([fe67780](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fe67780))
* **components:** fix correct update of reviewer ([b4ac0ec](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b4ac0ec))
* **components:** fix correct update of reviewer ([4a26556](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4a26556))
* **components:** fix correct update of reviewer ([bc8ef13](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bc8ef13))
* **components:** fix correct update of reviewer ([a30f25e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a30f25e))
* **components:** fix correct update of reviewer ([fb692e9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fb692e9))
* **components:** fix correct update of reviewer ([be3ce7b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be3ce7b))
* **components:** fix correct update of reviewer ([b700d30](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b700d30))
* **components:** fix correct update of reviewer ([4c9a7d9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4c9a7d9))
* **components:** invitation email ([2c3e61d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2c3e61d))
* **components:** invitation email ([b479847](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b479847))
* **components:** invitation email ([77a0ae4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/77a0ae4))
* **components:** invitation email ([f252b17](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f252b17))
* **components:** invitation email ([1ca6d60](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1ca6d60))
* **components:** invitation email ([e55a10a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e55a10a))
* **components:** invitation email ([3aab5d1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3aab5d1))
* **components:** invitation email ([ae51c4c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ae51c4c))
* **components:** invitation email ([f8bea7f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f8bea7f))
* **components:** invitation email ([b27816d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b27816d))
* **components:** invitation email ([74720ee](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/74720ee))
* **components:** invitation email ([da5d78d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/da5d78d))
* **components:** invitation email ([c2ef155](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2ef155))
* **components:** invitation email ([af6f384](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/af6f384))
* **components:** invitation email ([c2ebb60](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2ebb60))
* **components:** invitation email ([f557112](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f557112))
* **components:** invitation email ([8afcd9c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8afcd9c))
* **components:** invitation email ([f6d93cf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f6d93cf))
* **components:** invitation email ([860f233](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/860f233))
* **components:** invitation email ([087d151](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/087d151))
* **components:** invitation email ([75d3ac1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/75d3ac1))
* **components:** invitation email ([1e4bc4f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1e4bc4f))
* **components:** invitation email ([5d4d10e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5d4d10e))
* **components:** invitation email ([34d3054](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/34d3054))
* **components:** invitation email ([3d6b89b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3d6b89b))
* **components:** invitation email ([43a53a9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/43a53a9))
* **components:** invitation email ([fbac60d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fbac60d))
* **components:** invitation email ([d0d30af](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d0d30af))
* **components:** invitation email ([0da74f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0da74f6))
* **components:** invitation email ([6ee4a13](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6ee4a13))
* **components:** invitation email ([de5f871](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/de5f871))
* **components:** invitation email ([b8c9d1d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b8c9d1d))
* **components:** remove console ([ca53a45](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ca53a45))
* **components:** remove console ([ddfb7bc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ddfb7bc))
* **components:** remove console ([9653464](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9653464))
* **components:** remove console ([ff1da0e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ff1da0e))
* **components:** remove console ([36143dc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/36143dc))
* **components:** remove console ([4701387](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4701387))
* **components:** remove console ([69f0d81](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/69f0d81))
* **components:** remove console ([f118e50](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f118e50))
* **components:** remove console ([c3d6adb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c3d6adb))
* **components:** remove console ([5fd5969](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5fd5969))
* **components:** remove console ([ee993c6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee993c6))
* **components:** remove console ([32bcb22](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/32bcb22))
* **components:** remove console ([4394268](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4394268))
* **components:** remove console ([4b14588](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4b14588))
* **components:** remove console ([ce03041](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ce03041))
* **components:** remove console ([f639d2c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f639d2c))
* **components:** remove console ([9413dcf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9413dcf))
* **components:** remove console ([cfde302](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cfde302))
* **components:** remove console ([aaf36d2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aaf36d2))
* **components:** remove console ([29841d9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/29841d9))
* **components:** remove console ([842e654](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/842e654))
* **components:** remove console ([8eca1a1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8eca1a1))
* **components:** remove console ([f3cf5a0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f3cf5a0))
* **components:** remove console ([88c5b82](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/88c5b82))
* **components:** remove console ([7517e78](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7517e78))
* **components:** remove console ([050607f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/050607f))
* **components:** remove console ([20093f8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/20093f8))
* **components:** remove console ([ad079d5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ad079d5))
* **components:** remove console ([d0d32f4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d0d32f4))
* **components:** remove console ([ec4c2cb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ec4c2cb))
* **components:** remove console ([b04eb5d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b04eb5d))
* **components:** remove console ([fb5e7c2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fb5e7c2))
* **components:** write test for api/make-invitation ([a8ba38e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a8ba38e))
* **components:** write test for api/make-invitation ([c03f745](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c03f745))
* **xpub-submit:** move GraphQL functionality into separate component ([cfb2a81](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cfb2a81))




<a name="5.2.0"></a>
# [5.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@5.1.0...pubsweet-components@5.2.0) (2018-04-11)


### Bug Fixes

* **components:** change title styling ([3c154ba](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3c154ba))
* bump standard dependency ([0c599db](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0c599db))


### Features

* **components:** add Link to control panel ([85458b9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85458b9))
* **components:** fix import add link ([dfe4818](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/dfe4818))




<a name="5.1.0"></a>
# [5.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@5.0.0...pubsweet-components@5.1.0) (2018-04-03)


### Bug Fixes

* **components:** check configuration missing ([98e96ec](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98e96ec))
* **components:** fix rebase conflicts ([987858d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/987858d))
* **components:** fix test backend ([1e647f7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1e647f7))
* **components:** make use of email component ([084be2f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/084be2f))
* **components:** submit change validation minSize ([6efbcf0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6efbcf0))


### Features

* **ink-backend:** allow the app to run even if ink config is missing ([467b1de](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/467b1de)), closes [#351](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/351)
* **ink-backend:** improve error messages for missing config ([ccd6326](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ccd6326)), closes [#351](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/351)




<a name="5.0.0"></a>
# [5.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@4.3.0...pubsweet-components@5.0.0) (2018-03-30)


### Features

* **components:** remove react-bootstrap ([e66c933](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e66c933))
* **users-manager:** add support for removing members ([bb06148](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bb06148))
* **users-manager:** add way to add global teams ([9bbccab](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9bbccab))


### BREAKING CHANGES

* **users-manager:** Depends on a validation change for teamType -> string, and additionally, a validation change where team's objects are no longer required.




<a name="4.3.0"></a>
# [4.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@4.2.1...pubsweet-components@4.3.0) (2018-03-28)


### Features

* **client:** add Apollo Client ([2fe9d93](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2fe9d93))




<a name="4.2.1"></a>
## [4.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@4.2.0...pubsweet-components@4.2.1) (2018-03-28)




**Note:** Version bump only for package pubsweet-components

<a name="4.2.0"></a>
# [4.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@4.1.0...pubsweet-components@4.2.0) (2018-03-27)


### Bug Fixes

* **components:** review backend remove revs ([a2781e6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a2781e6))
* resolve remaining jsx-a11y lint issues ([0675289](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0675289))
* resolve remaining jsx-a11y lint issues ([a75c0de](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a75c0de))
* **components:** use version.id as key ([0ca2f56](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0ca2f56))
* **polling-server:** remove revs ([b42cecd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b42cecd))


### Features

* **components:** add Link from review page back to control panel ([860b737](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/860b737))
* **styleguide:** page per section ([0bf0836](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0bf0836))




<a name="4.1.0"></a>
# [4.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@4.0.0...pubsweet-components@4.1.0) (2018-03-19)


### Bug Fixes

* **component-send-email:** remove logger ([a8d2dda](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a8d2dda))
* **component-send-email:** remove sendMail callback ([d0be1d8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d0be1d8))
* **send-email:** rename for styleguide ([0a5b0a5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0a5b0a5))


### Features

* **component-send-email:** add error handling ([b3f423a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b3f423a))
* **component-send-email:** refactor send email ([201bfd9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/201bfd9))
* **component-send-email:** WIP refactor ses ([531d630](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/531d630))




<a name="4.0.0"></a>
# [4.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@3.0.2...pubsweet-components@4.0.0) (2018-03-15)


### Bug Fixes

* **login:** add missing recompose dependency ([a3b5a80](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a3b5a80)), closes [#353](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/353)
* **xpub:** variable linter error prosemirror ([f57768c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f57768c))


### Features

* **aws-s3:** add endpoint to zip S3 files of a manuscript ([f50f602](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f50f602))


### BREAKING CHANGES

* **aws-s3:** renamed the AWS-S3 endpoints to conform to REST principles (pluralize entity name)




<a name="3.0.2"></a>

## [3.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@3.0.1...pubsweet-components@3.0.2) (2018-03-09)

### Bug Fixes

* **xpub:** dubiously ignore linting errors ([a60d0ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a60d0ad))
* **xpub:** find-reviewers package name ([1b0ff2d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1b0ff2d))
* **xpub:** package name ([2383506](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2383506))
* **xpub:** tests ([cec85e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cec85e2))

<a name="3.0.1"></a>

## [3.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@3.0.0...pubsweet-components@3.0.1) (2018-03-06)

**Note:** Version bump only for package pubsweet-components

<a name="3.0.0"></a>

# [3.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@2.0.1...pubsweet-components@3.0.0) (2018-03-05)

### Bug Fixes

* restore FormGroup to its previous state, for later deletion ([3135ffd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3135ffd))
* **components:** add dependency on pubsweet/ui ([f0a1926](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f0a1926))
* **components:** correctly redirect when edit button is clicked ([faca509](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/faca509))
* **components:** login example ([6dfd66c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6dfd66c))
* **components:** login tests were failing after refactor ([62be047](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/62be047))
* **components:** make styleguide work (mostly) ([d036681](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d036681))
* **components:** PasswordReset was still using a CSS variable ([e1c2c84](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e1c2c84))
* **components:** signup and login error examples ([3f991ec](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3f991ec))
* **components:** styleguide can render components using validations ([93df7af](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/93df7af))

### Code Refactoring

* **ui:** tidy AppBar ([09751b6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/09751b6))

### Features

* **elife-theme:** add elife theme ([e406e0d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e406e0d))

### BREAKING CHANGES

* **ui:** \* navLinks prop is now navLinkComponents and expects an array of
elements

<a name="2.0.1"></a>

## [2.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@2.0.0...pubsweet-components@2.0.1) (2018-02-23)

**Note:** Version bump only for package pubsweet-components

<a name="2.0.0"></a>

# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@1.1.0...pubsweet-components@2.0.0) (2018-02-16)

### Bug Fixes

* **component:** fix tests ([bf9d13e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bf9d13e))

### Code Refactoring

* **components:** update mail transport config shape ([d142cd3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d142cd3))

### Features

* **component:** add aws ses package ([2e34627](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2e34627))
* **component:** sortable list component with react-dnd ([f4bda90](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f4bda90))

### BREAKING CHANGES

* **components:** mail transport config has moved from `mail-transport` to `mailer.transport`

<a name="1.1.0"></a>

# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@1.0.0...pubsweet-components@1.1.0) (2018-02-08)

### Bug Fixes

* **components:** update react-router-redux version to match client ([3d257ef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3d257ef))

### Features

* **components:** added aws s3 ([73c0764](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/73c0764))

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@0.0.12...pubsweet-components@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16

<a name="0.0.12"></a>

## [0.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-components@0.0.11...pubsweet-components@0.0.12) (2018-02-02)

**Note:** Version bump only for package pubsweet-components
