# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.3.7"></a>
## [0.3.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-draft@0.3.6...pubsweet-component-draft@0.3.7) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-draft

<a name="0.3.6"></a>
## [0.3.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-draft@0.3.5...pubsweet-component-draft@0.3.6) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-draft

<a name="0.3.5"></a>
## [0.3.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-draft@0.3.4...pubsweet-component-draft@0.3.5) (2018-03-05)


### Bug Fixes

* **components:** make styleguide work (mostly) ([d036681](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d036681))




<a name="0.3.4"></a>

## [0.3.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-draft@0.3.3...pubsweet-component-draft@0.3.4) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-draft
