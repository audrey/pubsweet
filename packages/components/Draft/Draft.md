Draft.js text editor

```js
const dummy = () => new Promise(() => null)

;<Draft
  id="1"
  blog={{ id: '2' }}
  fragment={{}}
  actions={{ getCollections: dummy, updateFragment: dummy }}
/>
```
