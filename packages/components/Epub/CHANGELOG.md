# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.15"></a>
## [0.2.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.14...pubsweet-component-epub@0.2.15) (2018-05-18)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.2.14"></a>
## [0.2.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.13...pubsweet-component-epub@0.2.14) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.2.13"></a>
## [0.2.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.12...pubsweet-component-epub@0.2.13) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.2.12"></a>

## [0.2.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.11...pubsweet-component-epub@0.2.12) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-epub
