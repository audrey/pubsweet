# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.4...pubsweet-component-epub-frontend@0.1.5) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-epub-frontend

<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.3...pubsweet-component-epub-frontend@0.1.4) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-epub-frontend

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub-frontend@0.1.2...pubsweet-component-epub-frontend@0.1.3) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-epub-frontend
