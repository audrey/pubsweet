# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.4"></a>
## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-medium-draft@0.2.3...pubsweet-component-medium-draft@0.2.4) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-medium-draft

<a name="0.2.3"></a>

## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-medium-draft@0.2.2...pubsweet-component-medium-draft@0.2.3) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-medium-draft
