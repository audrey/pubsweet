# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.4"></a>
## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-backend@1.0.3...pubsweet-component-password-reset-backend@1.0.4) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-password-reset-backend

<a name="1.0.3"></a>
## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-backend@1.0.2...pubsweet-component-password-reset-backend@1.0.3) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-password-reset-backend

<a name="1.0.2"></a>
## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-backend@1.0.1...pubsweet-component-password-reset-backend@1.0.2) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-password-reset-backend

<a name="1.0.1"></a>
## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-backend@1.0.0...pubsweet-component-password-reset-backend@1.0.1) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-password-reset-backend

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-backend@0.2.2...pubsweet-component-password-reset-backend@1.0.0) (2018-02-16)

### Code Refactoring

* **components:** update mail transport config shape ([d142cd3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d142cd3))

### BREAKING CHANGES

* **components:** mail transport config has moved from `mail-transport` to `mailer.transport`
