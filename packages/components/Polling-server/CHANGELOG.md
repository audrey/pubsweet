# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.4...@pubsweet/component-polling-server@0.0.5) (2018-04-03)




**Note:** Version bump only for package @pubsweet/component-polling-server

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.3...@pubsweet/component-polling-server@0.0.4) (2018-03-27)


### Bug Fixes

* **polling-server:** remove revs ([b42cecd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b42cecd))




<a name="0.0.3"></a>
## [0.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.2...@pubsweet/component-polling-server@0.0.3) (2018-03-19)




**Note:** Version bump only for package @pubsweet/component-polling-server
