# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.0.3"></a>
## [0.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-client@0.0.2...@pubsweet/component-polling-client@0.0.3) (2018-04-03)




**Note:** Version bump only for package @pubsweet/component-polling-client

<a name="0.0.2"></a>

## [0.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-client@0.0.1...@pubsweet/component-polling-client@0.0.2) (2018-02-16)

**Note:** Version bump only for package @pubsweet/component-polling-client
