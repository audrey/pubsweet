# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-sortable-list@0.1.2...pubsweet-component-sortable-list@0.1.3) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-sortable-list

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-sortable-list@0.1.1...pubsweet-component-sortable-list@0.1.2) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-sortable-list

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-sortable-list@0.1.0...pubsweet-component-sortable-list@0.1.1) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-sortable-list

<a name="0.1.0"></a>

# 0.1.0 (2018-02-16)

### Features

* **component:** sortable list component with react-dnd ([f4bda90](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f4bda90))
