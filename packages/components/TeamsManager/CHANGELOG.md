# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.1.8"></a>
## [1.1.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.7...pubsweet-component-teams-manager@1.1.8) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.7"></a>
## [1.1.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.6...pubsweet-component-teams-manager@1.1.7) (2018-05-18)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.6"></a>
## [1.1.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.5...pubsweet-component-teams-manager@1.1.6) (2018-05-10)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.5"></a>
## [1.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.4...pubsweet-component-teams-manager@1.1.5) (2018-05-09)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.4"></a>
## [1.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.3...pubsweet-component-teams-manager@1.1.4) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.3"></a>
## [1.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.2...pubsweet-component-teams-manager@1.1.3) (2018-04-24)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.2"></a>
## [1.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.1...pubsweet-component-teams-manager@1.1.2) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.1"></a>
## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.0...pubsweet-component-teams-manager@1.1.1) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.0"></a>
# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.8...pubsweet-component-teams-manager@1.1.0) (2018-03-30)


### Features

* **components:** remove react-bootstrap ([e66c933](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e66c933))




<a name="1.0.8"></a>
## [1.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.7...pubsweet-component-teams-manager@1.0.8) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.7"></a>
## [1.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.6...pubsweet-component-teams-manager@1.0.7) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.6"></a>
## [1.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.5...pubsweet-component-teams-manager@1.0.6) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.5"></a>
## [1.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.4...pubsweet-component-teams-manager@1.0.5) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.4"></a>

## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.3...pubsweet-component-teams-manager@1.0.4) (2018-03-09)

**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.3"></a>

## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.2...pubsweet-component-teams-manager@1.0.3) (2018-03-06)

**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.2"></a>

## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.1...pubsweet-component-teams-manager@1.0.2) (2018-03-05)

**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.1"></a>

## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.0...pubsweet-component-teams-manager@1.0.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@0.3.2...pubsweet-component-teams-manager@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
