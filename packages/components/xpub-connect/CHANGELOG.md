# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.0.16"></a>
## [0.0.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.15...xpub-connect@0.0.16) (2018-05-21)




**Note:** Version bump only for package xpub-connect

<a name="0.0.15"></a>
## [0.0.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.14...xpub-connect@0.0.15) (2018-05-18)




**Note:** Version bump only for package xpub-connect

<a name="0.0.14"></a>
## [0.0.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.13...xpub-connect@0.0.14) (2018-05-10)




**Note:** Version bump only for package xpub-connect

<a name="0.0.13"></a>
## [0.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.12...xpub-connect@0.0.13) (2018-05-09)




**Note:** Version bump only for package xpub-connect

<a name="0.0.12"></a>
## [0.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.11...xpub-connect@0.0.12) (2018-05-03)




**Note:** Version bump only for package xpub-connect

<a name="0.0.11"></a>
## [0.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.10...xpub-connect@0.0.11) (2018-04-24)




**Note:** Version bump only for package xpub-connect

<a name="0.0.10"></a>
## [0.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.9...xpub-connect@0.0.10) (2018-04-11)




**Note:** Version bump only for package xpub-connect

<a name="0.0.9"></a>
## [0.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.8...xpub-connect@0.0.9) (2018-04-03)




**Note:** Version bump only for package xpub-connect

<a name="0.0.8"></a>
## [0.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.7...xpub-connect@0.0.8) (2018-03-30)




**Note:** Version bump only for package xpub-connect

<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.6...xpub-connect@0.0.7) (2018-03-28)




**Note:** Version bump only for package xpub-connect

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.5...xpub-connect@0.0.6) (2018-03-27)




**Note:** Version bump only for package xpub-connect

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.4...xpub-connect@0.0.5) (2018-03-19)




**Note:** Version bump only for package xpub-connect

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-connect@0.0.3...xpub-connect@0.0.4) (2018-03-15)




**Note:** Version bump only for package xpub-connect

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package xpub-connect
