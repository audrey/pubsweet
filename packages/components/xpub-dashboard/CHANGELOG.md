# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.3.2"></a>
## [0.3.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.3.1...pubsweet-component-xpub-dashboard@0.3.2) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.3.1"></a>
## [0.3.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.3.0...pubsweet-component-xpub-dashboard@0.3.1) (2018-05-18)


### Bug Fixes

* **components:** upload diasble during converting ([227b136](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/227b136))




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.4...pubsweet-component-xpub-dashboard@0.3.0) (2018-05-10)


### Bug Fixes

* **components:** dashboard if statment reject ([999587a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/999587a))
* **components:** linter ([9aac3fa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9aac3fa))
* **components:** redirect submission add selectors ([53db5a7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/53db5a7))


### Features

* **components:** create accordion component ([54f5b7d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/54f5b7d))




<a name="0.2.4"></a>
## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.3...pubsweet-component-xpub-dashboard@0.2.4) (2018-05-09)


### Bug Fixes

* **xpub-dash:** fix reviewer item crash when status is revision ([fc79496](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fc79496))




<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.2...pubsweet-component-xpub-dashboard@0.2.3) (2018-05-03)


### Bug Fixes

* **xpub-dashboard:** correct styles for author manuscripts ([1d8761e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1d8761e))




<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.1...pubsweet-component-xpub-dashboard@0.2.2) (2018-05-03)


### Bug Fixes

* **components:** load all users to control panel ([90c88e6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/90c88e6))
* **components:** remove from Dashboard assign editor ([751a63e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/751a63e))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.0...pubsweet-component-xpub-dashboard@0.2.1) (2018-04-24)


### Bug Fixes

* **components:** add file streamlined data ([9dd6797](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9dd6797))
* **components:** add metadata StreamLined ([29a1fcd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/29a1fcd))
* **components:** add subinfo to upload ([446fc16](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/446fc16))
* **components:** change order ([2020d49](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2020d49))
* **components:** change text to create submission button ([d3b6385](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d3b6385))
* **components:** statuses changed for revision ([3bc09dc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3bc09dc))
* **dashboard:** change stremlined metadata label ([992cc4f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/992cc4f))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.3...pubsweet-component-xpub-dashboard@0.2.0) (2018-04-11)


### Bug Fixes

* **components:** change title styling ([3c154ba](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3c154ba))


### Features

* **components:** add Link to control panel ([85458b9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85458b9))
* **components:** fix import add link ([dfe4818](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/dfe4818))




<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.2...pubsweet-component-xpub-dashboard@0.1.3) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.1...pubsweet-component-xpub-dashboard@0.1.2) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.0...pubsweet-component-xpub-dashboard@0.1.1) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.5...pubsweet-component-xpub-dashboard@0.1.0) (2018-03-27)


### Features

* **styleguide:** page per section ([0bf0836](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0bf0836))




<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.4...pubsweet-component-xpub-dashboard@0.0.5) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.3...pubsweet-component-xpub-dashboard@0.0.4) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package pubsweet-component-xpub-dashboard
