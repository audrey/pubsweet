import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const Meta = styled.div`
  display: flex;
  flex-wrap: nowrap;
  font-size: ${th('fontSizeBaseSmall')};
  white-space: nowrap;
`

export default Meta
