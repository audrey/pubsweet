# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.1.4...xpub-edit@0.1.5) (2018-05-21)




**Note:** Version bump only for package xpub-edit

<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.1.3...xpub-edit@0.1.4) (2018-05-18)


### Bug Fixes

* use MIT on all package.json files ([4558ae4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4558ae4))




<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.1.2...xpub-edit@0.1.3) (2018-05-10)




**Note:** Version bump only for package xpub-edit

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.1.1...xpub-edit@0.1.2) (2018-05-09)


### Bug Fixes

* **xpub-edit:** ensure config is not regenerated on each render ([d98e722](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d98e722))




<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.1.0...xpub-edit@0.1.1) (2018-05-03)




**Note:** Version bump only for package xpub-edit

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.11...xpub-edit@0.1.0) (2018-05-03)


### Bug Fixes

* **xpub-edit:** prevent editors from submitting form ([f1f7dda](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f1f7dda))


### Features

* **xpub-edit:** create editor component with simple API ([ee6fb83](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee6fb83))




<a name="0.0.11"></a>
## [0.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.10...xpub-edit@0.0.11) (2018-04-24)


### Bug Fixes

* **components:** change placeholder ([d80a41a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d80a41a))
* **components:** structure title under Toolbar ([3e7b76b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3e7b76b))




<a name="0.0.10"></a>
## [0.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.9...xpub-edit@0.0.10) (2018-04-11)




**Note:** Version bump only for package xpub-edit

<a name="0.0.9"></a>
## [0.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.8...xpub-edit@0.0.9) (2018-04-03)




**Note:** Version bump only for package xpub-edit

<a name="0.0.8"></a>
## [0.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.7...xpub-edit@0.0.8) (2018-03-30)




**Note:** Version bump only for package xpub-edit

<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.6...xpub-edit@0.0.7) (2018-03-28)




**Note:** Version bump only for package xpub-edit

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.5...xpub-edit@0.0.6) (2018-03-27)




**Note:** Version bump only for package xpub-edit

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.4...xpub-edit@0.0.5) (2018-03-19)




**Note:** Version bump only for package xpub-edit

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-edit@0.0.3...xpub-edit@0.0.4) (2018-03-15)


### Bug Fixes

* **xpub:** variable linter error prosemirror ([f57768c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f57768c))




<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

### Bug Fixes

* **xpub:** dubiously ignore linting errors ([a60d0ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a60d0ad))
