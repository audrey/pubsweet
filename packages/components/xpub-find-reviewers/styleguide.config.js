module.exports = {
  title: 'xpub find reviewers style guide',
  styleguideComponents: {
    StyleGuideRenderer: require.resolve(
      'xpub-styleguide/src/components/StyleGuideRenderer',
    ),
    Wrapper: require.resolve('xpub-styleguide/src/components/Wrapper'),
  },
  context: {
    faker: 'faker',
  },
  skipComponentsWithoutExample: true,
  serverPort: 6062,
  theme: {
    fontFamily: {
      base: '"Fira Sans", sans-serif',
    },
    color: {
      link: 'cornflowerblue',
    },
  },
}
