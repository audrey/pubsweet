# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.4.2"></a>
## [0.4.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.4.1...pubsweet-component-xpub-review@0.4.2) (2018-05-21)


### Bug Fixes

* **components:** use Tabs from pubsweet ui ([8e9fd3c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8e9fd3c))




<a name="0.4.1"></a>
## [0.4.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.4.0...pubsweet-component-xpub-review@0.4.1) (2018-05-18)


### Bug Fixes

* **components:** authors assigning problem ([50baa94](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/50baa94))




<a name="0.4.0"></a>
# [0.4.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.3.2...pubsweet-component-xpub-review@0.4.0) (2018-05-10)


### Bug Fixes

* **components:** decision linter ([5679ce0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5679ce0))
* **components:** html parse, styled components ([8b24552](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8b24552))
* **components:** lint issues ([ff56878](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ff56878))
* **components:** redux form ([2f7f1ed](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2f7f1ed))
* **components:** review page layout ([4ea2cdd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4ea2cdd))


### Features

* **components:** add tabs to submission ([0e45892](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0e45892))




<a name="0.3.2"></a>
## [0.3.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.3.1...pubsweet-component-xpub-review@0.3.2) (2018-05-09)




**Note:** Version bump only for package pubsweet-component-xpub-review

<a name="0.3.1"></a>
## [0.3.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.3.0...pubsweet-component-xpub-review@0.3.1) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-xpub-review

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.2.0...pubsweet-component-xpub-review@0.3.0) (2018-05-03)


### Bug Fixes

* **components:** align columns cp page ([a5968b0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a5968b0))
* **components:** align dropdown horizontally ([a7081e3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a7081e3))
* **components:** change position and direction of assign ([7a7eeb3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7a7eeb3))
* **components:** change supplymentary to attachment component ([143c452](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/143c452))
* **components:** fix lint errors ([be173db](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be173db))
* **components:** fix lint errors ([c2b8e52](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2b8e52))
* **components:** fix lint errors ([98046fb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98046fb))
* **components:** fix lint errors ([2503ff9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2503ff9))
* **components:** fix unsued lint error ([20c282c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/20c282c))
* **components:** load all users to control panel ([90c88e6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/90c88e6))
* **components:** load all users to control panel ([92dac6b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/92dac6b))
* **components:** load all users to control panel ([f20e44d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f20e44d))
* **components:** load all users to control panel ([85fa14f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85fa14f))
* **components:** loading data in the decision form ([8f499aa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8f499aa))
* **components:** take care of case of zero files ([b70f728](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b70f728))


### Features

* **components:** add assign editors to cp ([3cca44d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3cca44d))
* **components:** add assign editors to cp ([987310d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/987310d))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.1.4...pubsweet-component-xpub-review@0.2.0) (2018-04-24)


### Bug Fixes

* **compoenents:** fix cases of empty objects in metadata ([7a5bfbc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7a5bfbc))
* **components:** change placeholder ([d80a41a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d80a41a))
* **components:** change text input fields ([775a961](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/775a961))
* **components:** change value to files at upload components ([aa2b45e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aa2b45e))
* **components:** fix file name problem ([73f33c9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/73f33c9))
* **components:** passport through route ([fdf9dce](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fdf9dce))
* **components:** passport through route ([593eeda](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/593eeda))
* **components:** remove tables - add list of metadata ([b9d57cd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b9d57cd))


### Features

* **component:** add make invitation request ([659eb64](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/659eb64))
* **component:** add make invitation request ([44f1574](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/44f1574))
* **component:** add make invitation request ([7245d35](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7245d35))
* **component:** add make invitation request ([bda7d95](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bda7d95))
* **component:** add make invitation request ([b049aa3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b049aa3))
* **component:** add make invitation request ([36faf21](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/36faf21))
* **component:** add make invitation request ([5a9b393](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5a9b393))
* **component:** add make invitation request ([ff3f8fb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ff3f8fb))
* **component:** add make invitation request ([335d0f0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/335d0f0))
* **component:** add make invitation request ([d070bb1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d070bb1))
* **component:** add make invitation request ([f9cae33](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f9cae33))
* **component:** add make invitation request ([947c846](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/947c846))
* **component:** add make invitation request ([38e5728](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/38e5728))
* **component:** add make invitation request ([430e9e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/430e9e2))
* **component:** add make invitation request ([30193b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/30193b3))
* **component:** add make invitation request ([721bbaf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/721bbaf))
* **component:** add make invitation request ([c4317bb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c4317bb))
* **component:** add make invitation request ([ae785b9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ae785b9))
* **component:** add make invitation request ([9d0ad57](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9d0ad57))
* **component:** add make invitation request ([1412d87](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1412d87))
* **component:** add make invitation request ([1911bab](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1911bab))
* **component:** add make invitation request ([6f27a3e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6f27a3e))
* **component:** add make invitation request ([3817c8a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3817c8a))
* **component:** add make invitation request ([61a1f0b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/61a1f0b))
* **component:** add make invitation request ([4820e45](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4820e45))
* **component:** add make invitation request ([27e2984](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/27e2984))
* **component:** add make invitation request ([9e00e11](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9e00e11))
* **component:** add make invitation request ([3d13943](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3d13943))
* **component:** add make invitation request ([9d731d3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9d731d3))
* **component:** add make invitation request ([251381c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/251381c))
* **component:** add make invitation request ([dcd1f46](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/dcd1f46))
* **component:** add make invitation request ([20628a4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/20628a4))
* **components:** add API endpoint invitation ([4739b84](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4739b84))
* **components:** add API endpoint invitation ([9276ef9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9276ef9))
* **components:** add API endpoint invitation ([24810b8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/24810b8))
* **components:** add API endpoint invitation ([16bf6a1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/16bf6a1))
* **components:** add API endpoint invitation ([d19bf84](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d19bf84))
* **components:** add API endpoint invitation ([ee82d6b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee82d6b))
* **components:** add API endpoint invitation ([17d9532](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/17d9532))
* **components:** add API endpoint invitation ([009b693](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/009b693))
* **components:** add API endpoint invitation ([e50383f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e50383f))
* **components:** add API endpoint invitation ([19932d7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/19932d7))
* **components:** add API endpoint invitation ([fc40c17](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fc40c17))
* **components:** add API endpoint invitation ([3c5cbea](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3c5cbea))
* **components:** add API endpoint invitation ([d21afe6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d21afe6))
* **components:** add API endpoint invitation ([d06ae4b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d06ae4b))
* **components:** add API endpoint invitation ([2703f3e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2703f3e))
* **components:** add API endpoint invitation ([d6bb84a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d6bb84a))
* **components:** add API endpoint invitation ([24ee6ca](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/24ee6ca))
* **components:** add API endpoint invitation ([f963b76](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f963b76))
* **components:** add API endpoint invitation ([48e8e12](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/48e8e12))
* **components:** add API endpoint invitation ([ae7d9aa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ae7d9aa))
* **components:** add API endpoint invitation ([c6d0d94](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c6d0d94))
* **components:** add API endpoint invitation ([db805c4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/db805c4))
* **components:** add API endpoint invitation ([d897187](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d897187))
* **components:** add API endpoint invitation ([daff65a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/daff65a))
* **components:** add API endpoint invitation ([9e9b4ce](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9e9b4ce))
* **components:** add API endpoint invitation ([0ca4cfa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0ca4cfa))
* **components:** add API endpoint invitation ([8de6954](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8de6954))
* **components:** add API endpoint invitation ([abdf121](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/abdf121))
* **components:** add API endpoint invitation ([a781136](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a781136))
* **components:** add API endpoint invitation ([10fb6e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/10fb6e2))
* **components:** add API endpoint invitation ([3fcc322](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3fcc322))
* **components:** add API endpoint invitation ([98d568e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98d568e))
* **components:** create invite reviewer endpoints ([fc6cad4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fc6cad4))
* **components:** fix correct update of reviewer ([6a423e4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6a423e4))
* **components:** fix correct update of reviewer ([4a26556](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4a26556))
* **components:** fix correct update of reviewer ([4c9a7d9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4c9a7d9))
* **components:** fix correct update of reviewer ([5988b36](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5988b36))
* **components:** fix correct update of reviewer ([b937db4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b937db4))
* **components:** fix correct update of reviewer ([48a70a8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/48a70a8))
* **components:** fix correct update of reviewer ([b22a250](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b22a250))
* **components:** fix correct update of reviewer ([c030cce](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c030cce))
* **components:** fix correct update of reviewer ([b4ac0ec](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b4ac0ec))
* **components:** fix correct update of reviewer ([d7553ef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d7553ef))
* **components:** fix correct update of reviewer ([a30f25e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a30f25e))
* **components:** fix correct update of reviewer ([c1b734e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c1b734e))
* **components:** fix correct update of reviewer ([ec31850](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ec31850))
* **components:** fix correct update of reviewer ([fe67780](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fe67780))
* **components:** fix correct update of reviewer ([acd3a2d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/acd3a2d))
* **components:** fix correct update of reviewer ([dcd2f94](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/dcd2f94))
* **components:** fix correct update of reviewer ([ee775be](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee775be))
* **components:** fix correct update of reviewer ([bc8ef13](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bc8ef13))
* **components:** fix correct update of reviewer ([45b7375](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/45b7375))
* **components:** fix correct update of reviewer ([1b449ab](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1b449ab))
* **components:** fix correct update of reviewer ([be3ce7b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be3ce7b))
* **components:** fix correct update of reviewer ([b700d30](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b700d30))
* **components:** fix correct update of reviewer ([30559e0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/30559e0))
* **components:** fix correct update of reviewer ([358a8f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/358a8f6))
* **components:** fix correct update of reviewer ([3d2d412](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3d2d412))
* **components:** fix correct update of reviewer ([d5de1d0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d5de1d0))
* **components:** fix correct update of reviewer ([fb692e9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fb692e9))
* **components:** fix correct update of reviewer ([11c44ae](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/11c44ae))
* **components:** fix correct update of reviewer ([46fdc54](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/46fdc54))
* **components:** fix correct update of reviewer ([b42550f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b42550f))
* **components:** fix correct update of reviewer ([2476402](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2476402))
* **components:** fix correct update of reviewer ([895bc73](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/895bc73))
* **components:** invitation email ([da5d78d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/da5d78d))
* **components:** invitation email ([b479847](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b479847))
* **components:** invitation email ([f6d93cf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f6d93cf))
* **components:** invitation email ([6ee4a13](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6ee4a13))
* **components:** invitation email ([2c3e61d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2c3e61d))
* **components:** invitation email ([b27816d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b27816d))
* **components:** invitation email ([af6f384](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/af6f384))
* **components:** invitation email ([1ca6d60](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1ca6d60))
* **components:** invitation email ([e55a10a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e55a10a))
* **components:** invitation email ([de5f871](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/de5f871))
* **components:** invitation email ([087d151](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/087d151))
* **components:** invitation email ([43a53a9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/43a53a9))
* **components:** invitation email ([74720ee](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/74720ee))
* **components:** invitation email ([f252b17](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f252b17))
* **components:** invitation email ([ae51c4c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ae51c4c))
* **components:** invitation email ([1e4bc4f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1e4bc4f))
* **components:** invitation email ([f557112](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f557112))
* **components:** invitation email ([34d3054](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/34d3054))
* **components:** invitation email ([d0d30af](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d0d30af))
* **components:** invitation email ([77a0ae4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/77a0ae4))
* **components:** invitation email ([75d3ac1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/75d3ac1))
* **components:** invitation email ([3aab5d1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3aab5d1))
* **components:** invitation email ([f8bea7f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f8bea7f))
* **components:** invitation email ([c2ef155](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2ef155))
* **components:** invitation email ([fbac60d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fbac60d))
* **components:** invitation email ([8afcd9c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8afcd9c))
* **components:** invitation email ([860f233](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/860f233))
* **components:** invitation email ([5d4d10e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5d4d10e))
* **components:** invitation email ([b8c9d1d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b8c9d1d))
* **components:** invitation email ([3d6b89b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3d6b89b))
* **components:** invitation email ([0da74f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0da74f6))
* **components:** invitation email ([c2ebb60](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2ebb60))




<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.1.3...pubsweet-component-xpub-review@0.1.4) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-xpub-review

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.1.2...pubsweet-component-xpub-review@0.1.3) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-xpub-review

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.1.1...pubsweet-component-xpub-review@0.1.2) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-xpub-review

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.1.0...pubsweet-component-xpub-review@0.1.1) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-xpub-review

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.0.5...pubsweet-component-xpub-review@0.1.0) (2018-03-27)


### Bug Fixes

* **components:** use version.id as key ([0ca2f56](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0ca2f56))


### Features

* **components:** add Link from review page back to control panel ([860b737](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/860b737))
* **styleguide:** page per section ([0bf0836](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0bf0836))




<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.0.4...pubsweet-component-xpub-review@0.0.5) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-xpub-review

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-review@0.0.3...pubsweet-component-xpub-review@0.0.4) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-xpub-review

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

### Bug Fixes

* **xpub:** dubiously ignore linting errors ([a60d0ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a60d0ad))
