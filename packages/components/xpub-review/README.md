## pubsweet-component-xpub-review

A PubSweet component that provides interfaces for editors to invite reviewers, reviewers to submit their reviews, and editors to make their decisions.

_Note:  
Some of the container components for these pages are a bit complex, as they need to also connect child components that are nested deeper in the tree and pass them down as props - there may be a better solution to this._
