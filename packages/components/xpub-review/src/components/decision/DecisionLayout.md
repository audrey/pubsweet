A page for an editor to make a decision on a version of a project.

```js
const { reduxForm } = require('redux-form')

const AssignEditor = require('../assignEditors/AssignEditor').default

const project = {
  id: faker.random.uuid(),
  reviewers: [
    {
      id: 'reviewer-invited',
      name: faker.name.findName(),
    },
    {
      id: 'reviewer-accepted',
      name: faker.name.findName(),
    },
    {
      id: 'reviewer-reviewed',
      name: faker.name.findName(),
      ordinal: 1,
    },
  ],
}

const versions = [
  {
    id: faker.random.uuid(),
    submitted: faker.date.past(2),
    declarations: {
      openReview: true,
    },
    files: {
      supplementary: [],
    },
    notes: {
      specialInstructions: 'foo'
    },
    reviewers: [
      {
        id: faker.random.uuid(),
        reviewer: 'reviewer-reviewed',
        status: 'reviewed',
        events: {
          invited: faker.date.past(2),
          accepted: faker.date.past(2),
          reviewed: faker.date.past(2),
        },
        note: {
          content: '<p>This is a review</p>',
        },
        recommendation: 'accept',
      },
    ],
    decision: {
      submitted: faker.date.past(2),
      note: {
        content: '<p>This is a decision</p>',
        recommendation: 'accept',
      },
    },
  },
  {
    id: faker.random.uuid(),
    submitted: faker.date.past(1),
    declarations: {
      openReview: true,
    },
    notes: {
      specialInstructions: 'foo'
    },
    files: {
      supplementary: [],
    },
    reviewers: [
      {
        id: faker.random.uuid(),
        reviewer: 'reviewer-reviewed',
        status: 'reviewed',
        events: {
          invited: faker.date.past(1),
          accepted: faker.date.past(1),
          reviewed: faker.date.past(1),
        },
        note: {
          content: '<p>This is another review</p>',
        },
        recommendation: 'accept',
      },
    ],
    decision: {
      submitted: faker.date.past(1),
      note: {
        content: '<p>This is a decision</p>',
        recommendation: 'accept',
      },
    },
  },
  {
    id: faker.random.uuid(),
    submitted: faker.date.past(1),
    declarations: {
      openReview: true,
    },
    notes: {
      specialInstructions: 'foo'
    },
    files: {
      supplementary: [],
    },
    reviewers: [
      {
        id: faker.random.uuid(),
        reviewer: 'reviewer-reviewed',
        status: 'accepted',
        events: {
          invited: faker.date.past(1),
          accepted: faker.date.past(1),
        },
      },
    ],
  },
]

const team = {
  members: [],
}

const options = [
  {
    value: faker.random.uuid(),
    label: faker.internet.userName(),
  },
  {
    value: faker.random.uuid(),
    label: faker.internet.userName(),
  },
  {
    value: faker.random.uuid(),
    label: faker.internet.userName(),
  },
]

const AssignEditorContainer = ({
  project,
  teamName,
  teamTypeName,
  addUserToTeam,
}) => (
  <AssignEditor
    team={team}
    options={options}
    project={project}
    teamName={teamName}
    teamTypeName={teamTypeName}
    addUserToTeam={addUserToTeam}
  />
)

const currentVersion = versions[versions.length - 1]
const decision = currentVersion.decision

const ConnectedDecisionLayout = reduxForm({
  form: 'decision-layout',
  onSubmit: values => console.log(values),
  onChange: values => console.log(values),
})(DecisionLayout)
;<div style={{ position: 'relative', height: 600 }}>
  <ConnectedDecisionLayout
    project={project}
    versions={versions}
    currentVersion={currentVersion}
    initialValues={decision}
    uploadFile={() => {}}
    AssignEditor={AssignEditorContainer}
  />
</div>
```
