# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.2.2"></a>
## [1.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.2.1...pubsweet-component-xpub-submit@1.2.2) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="1.2.1"></a>
## [1.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.2.0...pubsweet-component-xpub-submit@1.2.1) (2018-05-18)


### Bug Fixes

* **components:** add tests to suggestions component ([50777b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/50777b3))
* **components:** rewrite conditional checks to more clean ([c41d79d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c41d79d))
* **components:** submit submitted versions ([48d07ee](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/48d07ee))




<a name="1.2.0"></a>
# [1.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.1.0...pubsweet-component-xpub-submit@1.2.0) (2018-05-10)


### Bug Fixes

* **components:** fix lint errors ([4e22ec1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4e22ec1))
* **components:** fix linting issues ([4385b58](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4385b58))
* **components:** fixes in linter ([7c31f6b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c31f6b))
* **components:** html parse, styled components ([8b24552](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8b24552))
* **components:** linter ([9aac3fa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9aac3fa))
* **components:** merge two commponets two one ([4e2ed76](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4e2ed76))
* **components:** redirect submission add selectors ([53db5a7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/53db5a7))
* **components:** review page layout ([4ea2cdd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4ea2cdd))
* **components:** take care of case of zero files ([82cff08](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/82cff08))
* **components:** title wording ([0c293f4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0c293f4))


### Features

* **components:** add columns to submission and tabs ([40470a0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/40470a0))
* **components:** add current version files ([4c77f3c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4c77f3c))
* **components:** add tabs to submission ([0e45892](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0e45892))
* **components:** create accordion component ([05a23e4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/05a23e4))
* **components:** create accordion component ([54f5b7d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/54f5b7d))




<a name="1.1.0"></a>
# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.0.0...pubsweet-component-xpub-submit@1.1.0) (2018-05-09)


### Bug Fixes

* fixed misnamed redux form props in authors input ([940edc0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/940edc0))
* fixed misnamed redux form props in authors input ([bb4af56](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bb4af56))
* fixed misnamed redux form props in authors input ([fb362b2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fb362b2))


### Features

* add AuthorsInput component ([f7d12b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f7d12b3))
* authors input, added padding around fields ([1e5d742](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1e5d742))
* authors input, fixed merge error ([c908fa4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c908fa4))
* authors input, fixed prettier errors ([0657143](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0657143))
* authors input,component  updated to ensure at least one author ([d43dd92](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d43dd92))
* list styles for authors input ([3f85bbd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3f85bbd))
* two inputs per line ([aa0544a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aa0544a))
* update MetadataFields to use AuthorsInput component ([fa1640e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fa1640e))
* update MetadataFields to use AuthorsInput component ([1baac87](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1baac87))
* update MetadataFields to use AuthorsInput component ([355f282](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/355f282))




<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.1.1...pubsweet-component-xpub-submit@1.0.0) (2018-05-03)


### Bug Fixes

* **theme:** remove warning color ([c0897c8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c0897c8))


### BREAKING CHANGES

* **theme:** might break components that used the warning colors




<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.1.0...pubsweet-component-xpub-submit@0.1.1) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.10...pubsweet-component-xpub-submit@0.1.0) (2018-04-24)


### Bug Fixes

* **compoenents:** fix cases of empty objects in metadata ([7a5bfbc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7a5bfbc))
* **component:** put striphtml function back to place ([2a69dca](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2a69dca))
* **components:** change value to files at upload components ([aa2b45e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aa2b45e))
* **xpub-submit:** use no-redux version of uploadFile ([cc904a3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cc904a3))


### Features

* **xpub-submit:** move GraphQL functionality into separate component ([cfb2a81](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cfb2a81))




<a name="0.0.10"></a>
## [0.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.9...pubsweet-component-xpub-submit@0.0.10) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.9"></a>
## [0.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.8...pubsweet-component-xpub-submit@0.0.9) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.8"></a>
## [0.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.7...pubsweet-component-xpub-submit@0.0.8) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.6...pubsweet-component-xpub-submit@0.0.7) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.5...pubsweet-component-xpub-submit@0.0.6) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.4...pubsweet-component-xpub-submit@0.0.5) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.3...pubsweet-component-xpub-submit@0.0.4) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package pubsweet-component-xpub-submit
