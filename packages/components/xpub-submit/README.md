## pubsweet-component-xpub-submit

A PubSweet component that provides the form which the author completes before submitting a manuscript. It uses redux-form to handle the form state.

_Note:  
The previously-released version of this component used JSON configuration to describe the form, and that can be implemented here too, once we know more about the types of input that are needed in submission forms for other journals._
