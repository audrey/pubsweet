A dot representing the validation state of a form field.

### Idle

```js
;<Validot />
```

### Error

```js
;<Validot error message="There was an error" />
```

### Valid

```js
;<Validot valid />
```
