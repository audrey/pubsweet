## xpub-upload

A helper function for uploading a file.  
To be removed once [this](https://gitlab.coko.foundation/pubsweet/pubsweet-client/merge_requests/79/) is implemented in pubsweet-client.
