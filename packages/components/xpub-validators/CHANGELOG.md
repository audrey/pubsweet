# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.4...xpub-validators@0.0.5) (2018-04-03)




**Note:** Version bump only for package xpub-validators

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.3...xpub-validators@0.0.4) (2018-03-15)




**Note:** Version bump only for package xpub-validators

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package xpub-validators
