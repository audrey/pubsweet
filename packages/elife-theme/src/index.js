/* eslint-disable import/extensions */
import './fonts/index.css'

export default {
  /* Colors */
  colorBackground: '#fff',
  colorPrimary: '#0288d1',
  colorSecondary: '#888',
  colorFurniture: '#E0E0E0',
  colorBorder: '#aaa',
  colorBackgroundHue:
    '#f5f5f5' /* marginally darker shade of the app background so that it can be used to divide the interface when needed */,
  colorSuccess: '#629f43',
  colorError: '#b50000',
  colorText: '#212121',
  colorTextReverse: '#fff',
  colorTextPlaceholder: '#bdbdbd',

  /* Text variables */
  fontInterface: 'Georgia, serif',
  fontHeading: '"Avenir Next Webfont", Arial, Helvetica, sans-serif',
  fontReading: 'Georgia, serif',
  fontWriting: '"Courier 10 Pitch", Courier, monospace',
  fontSizeBase: '16px',
  fontSizeBaseSmall: '13px',
  fontSizeHeading1: '36px',
  fontSizeHeading2: '26px',
  fontSizeHeading3: '22px',
  fontSizeHeading4: '20px',
  fontSizeHeading5: '18px',
  fontSizeHeading6: '16px',
  fontLineHeight: '32px',

  /* Spacing */
  gridUnit: '24px',
  subGridUnit: '6px',

  /* Border */
  borderRadius: '3px',
  borderWidth: '1px',
  borderStyle: 'solid',

  /* Shadow (for tooltip) */
  boxShadow: '0 2px 4px 0 rgba(51, 51, 51, 0.3)',

  /* Transition */
  transitionDuration: '1s',
  transitionDurationM: '0.5s',
  transitionDurationS: '0.2s',
  transitionDurationXs: '0.1s',
  transitionTimingFunction: 'ease-in-out',
  transitionDelay: '500ms',
}
