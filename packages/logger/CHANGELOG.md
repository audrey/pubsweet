# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.2...@pubsweet/logger@0.2.3) (2018-05-18)


### Bug Fixes

* use one file at monorepo root ([456f49b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/456f49b))




<a name="0.2.2"></a>

## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.1...@pubsweet/logger@0.2.2) (2018-02-16)

**Note:** Version bump only for package @pubsweet/logger
