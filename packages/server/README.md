# Build status

[![build status](https://gitlab.coko.foundation/pubsweet/pubsweet-server/badges/master/build.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-server/builds)
[![coverage report](https://gitlab.coko.foundation/pubsweet/pubsweet-server/badges/master/coverage.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-server/commits/master)

# Description

This is the PubSweet server, to be used as a dependency in PubSweet apps.

# Development
- Check out our GitLab repository: https://gitlab.coko.foundation/pubsweet/pubsweet-server

# Roadmap
- Check out the [PubSweet 2.0 RFC](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/16)
