const supertest = require('supertest')
const app = require('../../src').configureApp(require('express')())
const STATUS = require('http-status-codes')
const querystring = require('querystring')

const COLLECTIONS_ROOT = '/api/collections/'

const authorizedRequest = (req, token) => {
  if (token) {
    req.set('Authorization', `Bearer ${token}`)
  }

  return req
}

const request = supertest(app)

// TODO: standardise parameter order of the "fragments" methods below

const fragments = {
  post: (opts = {}) => {
    const { fragment, collection, token } = opts

    let url

    if (collection) {
      const collectionId =
        typeof collection === 'object' ? collection.id : collection
      url = `/api/collections/${collectionId}/fragments`
    } else {
      url = '/api/fragments'
    }

    const req = request.post(url).send(fragment)

    return authorizedRequest(req, token)
  },
  patch: (opts = {}) => {
    const { fragmentId, update, token, collection } = opts

    let url
    if (collection) {
      url = `/api/collections/${collection.id}/fragments/${fragmentId}`
    } else {
      url = `/api/fragments/${fragmentId}`
    }

    const req = request.patch(url).send(update)

    return authorizedRequest(req, token)
  },
  get: (opts = {}) => {
    const { token, collection, fragmentId } = opts

    let url

    if (collection) {
      url = `/api/collections/${collection.id}/fragments`
    } else {
      url = '/api/fragments'
    }

    if (fragmentId) url += `/${fragmentId}`

    if (opts.fields) {
      url += `?${querystring.stringify({
        fields: opts.fields.join(','),
      })}`
    }

    const req = request.get(url)
    return authorizedRequest(req, token)
  },
  delete: (opts = {}) => {
    const { fragmentId, token } = opts
    const req = request.delete(`/api/fragments/${fragmentId}`)
    return authorizedRequest(req, token)
  },
  teams: (opts = {}) => {
    const { fragmentId, token } = opts

    const url = `/api/fragments/${fragmentId}/teams`

    const req = request.get(url)
    return authorizedRequest(req, token)
  },
}

const users = {
  authenticate: {
    post: (user, opts = {}) => {
      const expect = opts.expect === undefined ? true : opts.expect
      const token = opts.token === undefined ? true : opts.token

      let req = request.post('/api/users/authenticate').send({
        username: user.username,
        password: user.password,
      })

      if (expect) {
        req = req.expect(STATUS.CREATED)
      }

      if (token) {
        return req.then(res => res.body.token)
      }
      return req
    },
    get: token => {
      const req = request
        .get('/api/users/authenticate')
        .set('Authorization', `Bearer ${token}`)

      return req
    },
  },
  post: user => request.post('/api/users').send(user),
  // deprecated: use patch instead
  put: (userId, user, token) => {
    const req = request.put(`/api/users/${userId}`).send(user)

    return token ? req.set('Authorization', `Bearer ${token}`) : req
  },
  patch: (userId, user, token) => {
    const req = request.patch(`/api/users/${userId}`).send(user)

    return token ? req.set('Authorization', `Bearer ${token}`) : req
  },
  get: (opts = {}) => {
    const { token, userId } = opts
    const url = `/api/users${userId ? `/${userId}` : ''}`

    const req = request.get(url)

    return token ? req.set('Authorization', `Bearer ${token}`) : req
  },
  del: (userId, token) => {
    const req = request.delete(`/api/users/${userId}`)

    return token ? req.set('Authorization', `Bearer ${token}`) : req
  },
}

const collections = {
  list: (token, query = {}) => {
    const url = COLLECTIONS_ROOT

    if (query.fields) {
      query.fields = query.fields.join(',')
    }

    const req = request.get(url).query(query)
    return authorizedRequest(req, token)
  },
  create: (collection, token) => {
    const req = request.post(COLLECTIONS_ROOT).send(collection)
    return authorizedRequest(req, token)
  },
  retrieve: (collectionId, token) => {
    const req = request.get(COLLECTIONS_ROOT + collectionId)
    return authorizedRequest(req, token)
  },
  update: (collectionId, patch, token) => {
    const req = request.patch(COLLECTIONS_ROOT + collectionId).send(patch)
    return authorizedRequest(req, token)
  },
  delete: (collectionId, token) => {
    const req = request.delete(COLLECTIONS_ROOT + collectionId)
    return authorizedRequest(req, token)
  },
  listTeams: (collectionId, token) => {
    const req = request.get(`${COLLECTIONS_ROOT + collectionId}/teams`)
    return authorizedRequest(req, token)
  },
  listFragments: (collectionId, token, options) => {
    let url = `${COLLECTIONS_ROOT + collectionId}/fragments`

    if (options && options.fields) {
      url += `?${querystring.stringify({
        fields: options.fields.join(','),
      })}`
    }

    const req = request.get(url)
    return authorizedRequest(req, token)
  },
  createFragment: (collectionId, fragment, token) => {
    const req = request
      .post(`${COLLECTIONS_ROOT + collectionId}/fragments`)
      .send(fragment)
    return authorizedRequest(req, token)
  },
  retrieveFragment: (collectionId, fragmentId, token) => {
    const req = request.get(
      `${COLLECTIONS_ROOT + collectionId}/fragments/${fragmentId}`,
    )
    return authorizedRequest(req, token)
  },
  updateFragment: (collectionId, fragmentId, patch, token) => {
    const req = request
      .patch(`${COLLECTIONS_ROOT + collectionId}/fragments/${fragmentId}`)
      .send(patch)
    return authorizedRequest(req, token)
  },
  deleteFragment: (collectionId, fragmentId, token) => {
    const req = request.delete(
      `${COLLECTIONS_ROOT + collectionId}/fragments/${fragmentId}`,
    )
    return authorizedRequest(req, token)
  },
  listFragmentTeams: (collectionId, fragmentId, token) => {
    const req = request.get(
      `${COLLECTIONS_ROOT}${collectionId}/fragments/${fragmentId}/teams`,
    )
    return authorizedRequest(req, token)
  },
}

const teams = {
  get: (token, teamId) => {
    const url = `/api/teams/${teamId}`

    return request.get(url).set('Authorization', `Bearer ${token}`)
  },
  list: (token, collection) => {
    const collectionId = () =>
      typeof collection === 'object' ? collection.id : collection
    const collectionpart = collection ? `/collections/${collectionId()}` : ''

    const url = `/api${collectionpart}/teams`

    return request.get(url).set('Authorization', `Bearer ${token}`)
  },
  post: (team, token) => {
    const url = `/api/teams`

    return request
      .post(url)
      .send(team)
      .set('Authorization', `Bearer ${token}`)
  },
  patch: (team, teamId, token) => {
    const teamPart = teamId ? `/${teamId}` : ''
    const url = `/api/teams${teamPart}`

    return request
      .patch(url)
      .send(team)
      .set('Authorization', `Bearer ${token}`)
  },
}

const upload = {
  post: (file, token) => {
    const req = request.post('/api/upload').attach('file', file)

    return token ? req.set('Authorization', `Bearer ${token}`) : req
  },
  get: (path, token) => {
    const req = request.get(path)

    return token ? req.set('Authorization', `Bearer ${token}`) : req
  },
}

const graphql = {
  query: (query, variables, token) => {
    const req = request.post('/graphql').send({ query, variables })
    if (token) req.set('Authorization', `Bearer ${token}`)
    return req
  },
}

module.exports = {
  fragments,
  users,
  collections,
  teams,
  upload,
  graphql,
  request,
  app,
}
