# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.3.4"></a>
## [1.3.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.3.3...@pubsweet/styleguide@1.3.4) (2018-05-21)




**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.3.3"></a>
## [1.3.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.3.2...@pubsweet/styleguide@1.3.3) (2018-05-18)


### Bug Fixes

* use MIT on all package.json files ([4558ae4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4558ae4))




<a name="1.3.2"></a>
## [1.3.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.3.1...@pubsweet/styleguide@1.3.2) (2018-05-10)




**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.3.1"></a>
## [1.3.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.3.0...@pubsweet/styleguide@1.3.1) (2018-05-09)




**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.3.0"></a>
# [1.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.2.4...@pubsweet/styleguide@1.3.0) (2018-05-03)


### Bug Fixes

* **ui:** revert to default theme as default for styleguide ([ec2aabe](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ec2aabe))


### Features

* **theme:** coko theme is in place ([731f501](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/731f501))




<a name="1.2.4"></a>
## [1.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.2.3...@pubsweet/styleguide@1.2.4) (2018-04-24)




**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.2.3"></a>
## [1.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.2.2...@pubsweet/styleguide@1.2.3) (2018-04-11)




**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.2.2"></a>
## [1.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.2.1...@pubsweet/styleguide@1.2.2) (2018-03-30)




**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.2.1"></a>
## [1.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.2.0...@pubsweet/styleguide@1.2.1) (2018-03-28)


### Bug Fixes

* **styleguide:** save selected theme when switching between components ([be2b255](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be2b255)), closes [#362](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/362)




<a name="1.2.0"></a>
# [1.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.1.4...@pubsweet/styleguide@1.2.0) (2018-03-27)


### Bug Fixes

* **styleguide:** add lodash to styleguide deps ([f60f632](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f60f632))
* **styleguide:** styleguidist cannot cope with spaces in names ([19dfd4e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/19dfd4e))


### Features

* **styleguide:** add theme picker to styleguide ([27b3b05](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/27b3b05)), closes [#346](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/346)
* **styleguide:** add theme picker to styleguide ([3233a86](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3233a86)), closes [#346](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/346)
* **styleguide:** page per section ([0bf0836](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0bf0836))




<a name="1.1.4"></a>
## [1.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.1.3...@pubsweet/styleguide@1.1.4) (2018-03-19)




**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.1.3"></a>
## [1.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.1.2...@pubsweet/styleguide@1.1.3) (2018-03-15)




**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.1.2"></a>

## [1.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.1.1...@pubsweet/styleguide@1.1.2) (2018-03-09)

**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.1.1"></a>

## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.1.0...@pubsweet/styleguide@1.1.1) (2018-03-06)

**Note:** Version bump only for package @pubsweet/styleguide

<a name="1.1.0"></a>

# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@1.0.0...@pubsweet/styleguide@1.1.0) (2018-03-05)

### Bug Fixes

* **components:** make styleguide work (mostly) ([d036681](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d036681))
* **components:** styleguide can render components using validations ([93df7af](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/93df7af))
* **ui:** failing build ([c52f678](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c52f678))

### Features

* **default-theme:** add variables to default theme ([ba121b0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba121b0))
* **elife-theme:** add elife theme ([e406e0d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e406e0d))
* **normalize:** add normalize css ([9eb24e5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9eb24e5))
* **styleguide:** add button for viewing components against grid ([6a4999f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6a4999f))

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/styleguide@0.1.4...@pubsweet/styleguide@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
