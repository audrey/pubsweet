import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const ErrorText = styled.div`
  color: ${th('colorError')};
`

/**
 * @component
 */
export default ErrorText
